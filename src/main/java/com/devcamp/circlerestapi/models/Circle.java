package com.devcamp.circlerestapi.models;

public class Circle {
    private double radius = 1.0;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    // diện tích hình tròn
    public double getArea() {
        return Math.PI * Math.pow(this.radius, 2);
    }
    // chi vi hình tròn
    public double getCircumference() {
        return Math.PI * (this.radius * 2);
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

    
}
