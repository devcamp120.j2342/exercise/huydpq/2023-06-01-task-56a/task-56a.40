package com.devcamp.circlerestapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlerestapi.models.Circle;

@RequestMapping
@CrossOrigin
@RestController
public class CircleController {
    @GetMapping("/circle-area")
    public double circleRestApi(){
        Circle circle = new Circle(2);
       
        return circle.getArea();
    }

}
